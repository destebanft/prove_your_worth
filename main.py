# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

import requests
from bs4 import BeautifulSoup
from PIL import Image, ImageDraw

url_prove = 'http://www.proveyourworth.net/level3/'
url_start = url_prove + 'start'
url_payload = url_prove + 'payload'
session = requests.Session()


def get_cookie(url):
  session.get(url)
  return session.cookies.get('PHPSESSID')


def get_hash(url):
  request = session.get(url)
  soup = BeautifulSoup(request.text, 'html.parser')
  print(soup)
  return soup.find("input", {"name": "statefulhash"})['value']


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
  cookie = get_cookie(url_prove)
  hash = get_hash(url_prove)
  session.get('http://www.proveyourworth.net/level3/activate?statefulhash' + hash)
  request = session.get(url_payload, stream=True)
  image = Image.open(request.raw)
  draw = ImageDraw.Draw(image)
  draw.text((20, 20),
            'Name: {}\nHash: {}\nEmail: {}\n{}'.format('David Esteban Fajardo Torres', hash,
                                                       'destebanft@protonmail.com', 'Python Developer'),
            fill=(63, 13, 18, 255),
            )
  image.save("image.jpg", "JPEG")
  url_post = '{}'.format(request.headers['X-Post-Back-To'])
  files = {
    'code': open('main.py', 'rb'),
    'resume': open('CV_Esteban_Fajardo.pdf', 'rb'),
    'image': open('image.jpg', 'rb')
  }
  data = {
    'name': 'David Esteban Fajardo Torres',
    'email': 'destebanft@protonmail.com',
    'about_me': "Hi, I'm Esteban Fajardo, a passionate about technology with a lot of desire to learn",
    'code': '',
    'resume': '',
    'image': ''
  }
  request = session.post(url_post, data=data, files=files)
  print(request.status_code)
  print(request.text)
